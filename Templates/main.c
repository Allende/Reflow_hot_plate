#include "glcd.h"
#include "init.h"

/***************************************************************************************************
                                  Top Level Declaration   
***************************************************************************************************/
typedef enum handle{
    UP = 0,
    DOWN,
    INT,
    OUT,
    NONE
} t_handle;
typedef enum STATEX
{
    BEGIN = 0,
    LOAD,
    DEFINE,
    START,
    STOP,
    END,
    PANNEL,
    MENU ,
    SET,
    WHAIT,
    FAILED
}s_st_x;

typedef void (*f_void_x)(x_model*, t_handle);

typedef struct  
{
    int x;
} x_model;

typedef struct  
{
    /// @brief 
    s_st_x   state;
    f_void_x display;
} x_view;

typedef struct  
{
    t_handle h;
} x_event;

typedef struct 
{
    x_event* event;
    x_model* model;
    x_view*   view;

} x_controller;


/***************************************************************************************************
                                  Local Function Declaration   
***************************************************************************************************/
void console_display_x_begin(x_model* x, p)
{    
    static int f=1;
    static bool init= false;
    
    switch (p)
    {
    case UP:
        f=(f>1)?f--:MENU_MAX;
        GLCD_EnableInversion();
        switch (f)
        {
        case 1:
            GLCD_SetCursor(f,8);
            GLCD_Printf("LOAD\n");
            break;
        case 2:
            GLCD_SetCursor(f|,7);
            GLCD_Printf("DEFINE\n");
            break;
        case 3:
            GLCD_SetCursor(f|,8);
            GLCD_Printf("START\n");
            break;
        }   
        GLCD_DisableInversion();
        break;
    case DOWN:
        f=(f<MENU_MAX)?f++:1;
        GLCD_EnableInversion();
        switch (f)
        {
        case 1:
            GLCD_SetCursor(f,8);
            GLCD_Printf("LOAD\n");
            break;
        case 2:
            GLCD_SetCursor(f|,7);
            GLCD_Printf("DEFINE\n");
            break;
        case 3:
            GLCD_SetCursor(f|,8);
            GLCD_Printf("START\n");
            break;
        }
        GLCD_DisableInversion();  
        break;
    case INT:
        GLCD_EnableInversion();
        GLCD_GoToLine(f);   
        switch (f)
        {
        case 1:
            GLCD_Printf("        LOAD        \n");
            break;
        case 2:
            GLCD_Printf("       DEFINE        \n");
            break;
        case 3:
            GLCD_Printf("        START        \n");
            break;
        }   
        GLCD_DisableInversion();
        break;
    case OUT:
        /* code */
        break;
    default://"NONE"case 1:
        if (!init)
        {
            GLCD_ScrollMessage(0,"Welcome to Reflow again");
            GLCD_Printf("\n");
            GLCD_GoToLine(1);   
            GLCD_EnableInversion();
            GLCD_Printf("        LOAD        \n");
            GLCD_DisableInversion();
            GLCD_SetCursor(2,8);
            GLCD_Printf("DEFINE\n");
            GLCD_SetCursor(3,8);
            GLCD_Printf("START\n");
            GLCD_SetCursor(6,0);
            GLCD_Printf("----------------------\n");
            GLCD_Printf("<:out >:int v:up v:down\n");
            init++;
        }
        break;
    }
    return;

}
void console_display_x_load(x_model* x)
{
    printGLCD("%d\r\n",x->x);
}
void console_display_x_define(x_model* x)
{
    printGLCD("%d\r\n",x->x);
}
/************************************************************
    Factory Function Definition 
*************************************************************/
void create_console_view(x_view* this,t_state f)
{    
    switch (f)
    {
    case BEGIN:
        this->display = console_display_x_begin;
        break;
    case LOAD:
        this->display = console_display_x_load;
        break;
    case DEFINE:
        this->display = console_display_x_define;        
        break;
    case START:
        /* code */
        break;
    case STOP:
        /* code */
        break;                
    case END:
        /* code */
        break;
    case PANNEL:
        /* code */
        break;
    case MENU:
        /* code */
        break;
    case SET:
        /* code */
        break;
    case FAILED:
        /* code */
        break;             
    case WHAIT:
        /* code */
        break;
    default:
        break;
    }
}

void controller_update_data(x_controller* this, int x)
{
    this->model->x = x;
    this->view->display(this->model,this->event);
}

void x_controler_init(x_controller* this, x_model* model, x_view* view,x_event* event)
{
    this->model = model;
    this->view = view;
    this->event = event;
}

void menu(x_controller* this)
{
    static t_state f=BEGIN;
    switch (f)
    {
    case BEGIN:
        create_console_view(&this->view,f);
        controller_update_data(x_controller* this, null);   
        switch (this->event->h)
        {
        case UP :
            If(f=)
            break;
        case DOWN:
            /* code */
            break;
        case OUT:
            /* code */
            break;
        case INT:
            /* code */
            break;
        default:
            break;
        }     
        break;
    case LOAD:
        create_console_view(&this->view,f);
        break;
    case DEFINE:
        /* code */
        break;
    case START:
        /* code */
        break;
    case STOP:
        /* code */
        break;                
    case END:
        /* code */
        break;
    case PANNEL:
        /* code */
        break;
    case MENU:
        /* code */
        break;
    case SET:
        /* code */
        break;
    case FAILED:
        /* code */
        break;             
    case WHAIT:
        /* code */
        break;
    default:
        break;
    }
}

/***************************************************************************************************
                                Start the main program
***************************************************************************************************/
void main() 
{
    /* Initialize the glcd before displaying any thing on the lcd */
    GLCD_Init();

    x_view view;
    x_model model;
    x_event event;
    x_controller controller;

    x_controler_init(&controller, &model, &view, &event);

    controller_update_data(&controller, 24);

    while (TRUE);
    {
        menu(&controller);
        button(&controller);
    }

    
}