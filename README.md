# Reflow

![board dimage](Image/3d.jpeg)

# Sumary

The proyect is based at a hot plate NTC with 450W of power AC:

![board dimage](Image/plate.webp)

The main focus was a follow a hot curve like this:

![board dimage](Image/curve_hot.png)

In early designs ( continuos time ) the system respons for a ramp was:

![](Image/continuos_ramp_respons.jpg){width="530"}

In early designs ( discrete time ) the system respons for a work curve was:

![board dimage](Image/work_curve_z.jpg)

The final designg ( discrete time  and  with power supply PWM AC zero cross detection) the system respons for a work curve was:

![board dimage](Image/Real_simu.jpg)

# Reflow Controller and case based on MICROCHIP PIC18f2550 or simil.

You can read more about my project [here](https://hackaday.io/project/) at Hackaday.io . :slightly_smiling_face:

This repository contain firmware and 3D files for PIC18f reflow controller.

All version of controller include:

- PIC18f2550
- 128x64 LCD (ks)
- NTC120k IC for measuring temperature
- buzzer
- MOC
- TRIAC
- Power Supply
- USB to UART converter HT740.
- Isolate buffer.
- USB connector
- 4 buttons (Left, Right, Up, Down)
- status LEDs 

### There are these revision of the board:

- **rev. A**

First version of the board. Fully working, except buzzer.

- **rev. B**

Second version of the board. Reworked buzzer and changed for SMD version.

- **rev. C**

This board include 2 more buttons (Menu & Back button), fan connector, Reset button and USB-C connector.

PCB schematic
![board dimage](Image/schematic.png)
![board dimage](Doc/reflow.pdf)


Board dimensions are 105 x 51 mm. 

***

### There are 2 version of firmware for this board:

 The system is 2 degre and no any simplist PIDs is employed in this proyect.
 The design was based on linear controller and proportional driver control.
 The compensator have two poles and one zero.
 The firmware was designed based in objet-oriented desing(OOD) in C.
 The firmware was designed at MVC model to show a grafic interfase.

- **V1**

Basic functionality. One working profile which can be start/stoped.
**Not developed anymore.**

- **V2** (**recommended**)

You will need these libraries to compile FW from source code: 

 - GLC () (in Library Manager)
 - ArduinoJson (Library Manager)

#### This firmware include following:

- [x] OTA update.
- [x] Vertical/Horizontal mode.
- [x] WiFiManager - you can setup your WiFi network for OTA updates. When not setup, it will start in AP mode and create network with SSID **ReflowOvenAP**. Setup server can be access at IP address: **192.168.4.1**, after connecting to this AP.  

Planned features:

- [ ] Reflow profiles (via JSON file)
- [ ] Web server for remote change of the settings 

This firmware include Menu, which can be open by pressing **Menu** button (**Left** button on rev. **A&B**). Here you can change items in **Settings**. This firmware still contain one hardcoded profile. 

*** 

You can find STL files and Fusion 360 files for Front cover and for separate case (HW folder).  
									 
If you want to look at Front cover in 3D (web browser) you can use this link: <a href="https://a360.co/2S8YHzj" target="_blank">https://a360.co/2S8YHzj</a>

Front panel asssembly (rev A&B): <a href="https://a360.co/2S37lPS" target="_blank">https://a360.co/2S37lPS</a>

Firmware is based on Rocket Scream Electronics firmware for [Reflow Oven Shield](https://www.rocketscream.com/blog/2012/11/28/updated-back-in-stock-reflow-oven-shield-controller/)

Some parts of the V2 firmware are inspired by parts of <a href="https://github.com/UnexpectedMaker/ReflowMaster" target="_blank">Reflow Master</a> firmware by @UnexpectedMaker

In case that you will have any question, you can contact me at @gmail.com