/***************************************************************************************************
                                   ExploreEmbedded	
****************************************************************************************************
 * File:   nvm.h
 * Version: 1.0
 * Author: Sebastián Allende Lammens
 * Website: http://www.exploreembedded.com/wiki
 * Description: File contains the Function prototypes for the list routines
 

GNU GENERAL PUBLIC LICENSE: 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
Errors and omissions should be reported to cba.allende@gmail.com
 **************************************************************************************************/
 
 
 /***************************************************************************************************
                             Revision History
****************************************************************************************************				   
1.0: Initial version
***************************************************************************************************/
 
#ifndef _NVM_H
#define _NVM_H

//#include"stdutils.h"

/***************************************************************************************************
                              PORT Configuration
 ***************************************************************************************************/

/**************************************************************************************************/

/***************************************************************************************************
                             Commonly used macros/Constants
***************************************************************************************************/
#define STAGES 6
/***************************************************************************************************
                                 Struct/Enums used
 ***************************************************************************************************/
//typedef struct t_list t_list;
//typedef struct t_listVtb1 t_listVtb1;

/* struct| t_listVtb1
{
  void (*pointer  )(void *var);
  void (*set      )(void *var);
  void (*get      )(void *var);
  void (*clear    )(void *var);
  void (*clear_all)(void *var);
}; */

/***************************************************************************************************
                             Function prototypes
***************************************************************************************************/
void     NVM_Init (void);
void     NVM_Read (void *const );
void     NVM_Write(void *const );
/**************************************************************************************************/

#endif