/***************************************************************************************************/
#ifndef _GLCD_X_H_
#define _GLCD_X_H_

#include <stdarg.h>
#include "stdutils.h"
//#include "delay.h"

#include "glcd_x.h"

/***************************************************************************************************
                             Commonly used LCD macros/Constants
***************************************************************************************************/

#define C_MaxBarhist_U8 128

/***************************************************************************************************
                 PreCompile configurations to enable/disable the functions
****************************************************************************************************
PreCompile configuration to enable or disable the API's.
 1.Required interfaces can be enabled/disabled by configuring its respective macros to 1/0.
 2. By default all the API's are disabled.
 3. Displaying of floating number takes huge controller resources and need to be enabled only
    if required. This implies for other interfaces as well.
****************************************************************************************************/
#define Enable_GLCD_curve 1
#define Enable_GLCD_curve_or 1
#define Enable_GLCD_hist_line 1



/***************************************************************************************************
                             Function Prototypes
 ***************************************************************************************************/
void GLCD_curve(uint8_t *p1, uint8_t *p2);
void GLCD_aliasing_line(uint8_t *p1, uint8_t *p2);
void GLCD_hist_line(uint8_t var_barGraphNumber_u8, uint8_t var_percentageValue_u8, const uint8_t botton, const uint8_t upper);



#endif