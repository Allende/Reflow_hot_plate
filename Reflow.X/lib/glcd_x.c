
#include "glcd_x.h"

/***************************************************************************************************
                                  Local Function Definition
***************************************************************************************************/
/***************************************************************************************************
                       void GLCD_DisplayString(char *ptr_stringPointer_u8)
****************************************************************************************************
 * I/P Arguments: String(Address of the string) to be displayed.
 * Return value    : none

 * description  :
               This function is used to display the ASCII string on the lcd.
                 1.The ptr_stringPointer_u8 points to the first char of the string
                    and traverses till the end(NULL CHAR)and displays a char each time.

****************************************************************************************************/
void GLCD_curve(byte *p1, byte *p2)
{
  byte p = 0, k = 0;
  byte dv, m, gx, gy, l;

  dv = LIM_A % M_A;
  d = LIM_A / M_A;
  m = ;
  char h[2] = {null, null};
  for (byte i = 0; i < gx; i++)
  {
    switch (m)
    {
    case 0:
      h = 8;
      break;
    case 1:
      if (l < 8)
      {
        *h = 1 << l;
        l += m;
      }
      else
      {
        *h = 1;
        l = 0;
      }
      break;
    case 2:
      if (l + m < 8)
      {
        *h = 0b00000011 << l;
        l += m;
      }
      else
      {
        *h = 0b00000011 l = 0;
      }
      break;
    case 3:
      if (l + m < 8)
      {
        *h = 0b00000111 << l;
        l += m;
      }
      else
      {
        *h = 0b00000111 << l;
        l += m;
        h[1] = 0b00000111 >> (m - 8 + l);
        l = l - 7;
      }
      break;
    case 4:
      h = 0b00001111 << l;
      break;
    case 5:
      h = 0b00011111 << l;
      break;
    case 6:
      h = 0b00111111 << l;
      break;
    case 7:
      h = 0b01111111 << l;
      break;
    case 8:
      h = 0xFF break;
    }

    if (h[1] != null)
    {
      GLCD_SetPointer(j, i);
      GLCD_PrintDots(*h);
      GLCD_SetPointer(++j, i);
      GLCD_PrintDots(h[1]);
      h[1] = null;
    }
    else
    {
      GLCD_SetPointer(j++, i);
      GLCD_PrintDots(*h);
    }
  }
}
/************************************************************
 Function for alias text and figure
*************************************************************/
/***************************************************************************************************
                       void GLCD_DisplayString(char *ptr_stringPointer_u8)
****************************************************************************************************
 * I/P Arguments: String(Address of the string) to be displayed.
 * Return value    : none

 * description  :
               This function is used to display the ASCII string on the lcd.
                 1.The ptr_stringPointer_u8 points to the first char of the string
                    and traverses till the end(NULL CHAR)and displays a char each time.

****************************************************************************************************/
void GLCD_aliasing_line(byte *p1, byte *p2)
{
  byte p = 0, k = 0;
  byte dv, m, gx, gy, l;

  dv = LIM_A % M_A;
  d = LIM_A / M_A;
  m = ;
  char h[2] = {null, null};
  for (byte i = 0; i < gx; i++)
  {
    switch (m)
    {
    case 0:
      h = 8;
      break;
    case 1:
      if (l < 8)
      {
        *h = 1 << l;
        l += m;
      }
      else
      {
        *h = 1 l = 0;
      }
      break;
    case 2:
      if (l + m < 8)
      {
        *h = 0b00000011 << l;
        l += m;
      }
      else
      {
        *h = 0b00000011 l = 0;
      }
      break;
    case 3:
      if (l + m < 8)
      {
        *h = 0b00000111 << l;
        l += m;
      }
      else
      {
        *h = 0b00000111 << l;
        l += m;
        h[1] = 0b00000111 >> (m - 8 + l);
        l = l - 7;
      }
      break;
    case 4:
      h = 0b00001111 << l;
      break;
    case 5:
      h = 0b00011111 << l;
      break;
    case 6:
      h = 0b00111111 << l;
      break;
    case 7:
      h = 0b01111111 << l;
      break;
    case 8:
      h = 0xFF break;
    }

    if (h[1] != null)
    {
      GLCD_SetPointer(j, i);
      GLCD_PrintDots(*h);
      GLCD_SetPointer(++j, i);
      GLCD_PrintDots(h[1]);
      h[1] = null;
    }
    else
    {
      GLCD_SetPointer(j++, i);
      GLCD_PrintDots(*h);
    }
  }
}

/***************************************************************************************************
void GLCD_hist_line(uint8_t var_barGraphNumber_u8, uint8_t var_percentageValue_u8)
 ****************************************************************************************************
 * I/P Arguments   : var_barGraphNumber_u8 : position of bar ( 0 to 3 )
                     var_percentageValue_u8 : value of the bar in percentage(0-100)
 * Return value    : none

 * description  :
                This function displays bar graph in  direction.
 ****************************************************************************************************/

void GLCD_hist_line(uint8_t var_barGraphNumber_u8, uint8_t var_percentageValue_u8, const uint8_t botton, const uint8_t upper)
{
  uint8_t var_lineNumberToStartDisplay_u8, i, j, var_barGraphPosition_u8;
  uint8_t lineNumber, var_valueToDisplay_u8;

  if ((var_barGraphNumber_u8 < C_MaxBarhist_U8) && (var_percentageValue_u8 <= 100))
  {
    /* Divide the value by 8, as we have 8-pixels for each line */
    var_percentageValue_u8 /= 2;
    var_lineNumberToStartDisplay_u8 = (var_percentageValue_u8 >> 3);
    lineNumber = 7 - var_lineNumberToStartDisplay_u8;

    for (i = botton; i < upper; i++)
    {
      GLCD_SetCursor(i, (var_barGraphNumber_u8));
      if (i < lineNumber)
      {
        var_valueToDisplay_u8 = 0x00;
      }
      else if (i == lineNumber)
      {
        var_valueToDisplay_u8 = util_GetMod8(var_percentageValue_u8, 8);
        var_valueToDisplay_u8 = (0xff << (8 - var_valueToDisplay_u8));
      }
      else
      {
        var_valueToDisplay_u8 = 0xff;
      }

      for (j = 0; j < 12; j++)
      {
        glcd_DataWrite(var_valueToDisplay_u8);
      }
    }
  }
}
