/***************************************************************************************************
                                   ExploreEmbedded    
 ****************************************************************************************************
 * File:   eeprom.c
 * Version: 15.0
 * Author: ExploreEmbedded
 * Website: http://www.exploreembedded.com/wiki
 * Description: Contains the library routines for Eeprom Read-Write

The libraries have been tested on ExploreEmbedded development boards. We strongly believe that the 
library works on any of development boards for respective controllers. However, ExploreEmbedded 
disclaims any kind of hardware failure resulting out of usage of libraries, directly or indirectly.
Files may be subject to change without prior notice. The revision history contains the information 
related to updates. 


GNU GENERAL PUBLIC LICENSE: 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Errors and omissions should be reported to codelibraries@exploreembedded.com
 ***************************************************************************************************/


/***************************************************************************************************
                             Revision History
 ****************************************************************************************************
15.0: Initial version 
 ***************************************************************************************************/

#include "eeprom.h"
#include "nvm.h"
//#include "delay.h"
#include "xc.h"

/***************************************************************************************************
Local Function Declaration
***************************************************************************************************/

static uint8_t offset=0;

/***************************************************************************************************
  void NVM_Init(uint16_t v_eepromAddress_u16, uint8_t v_eepromData_u8)
 ***************************************************************************************************
 * I/P Arguments: uint16_t: eeprom_address at which eeprom_data is to be written
                  uint8_t: byte of data to be to be written in eeprom.
 * Return value    : none

 * description: This function is used to write the data at specified EEPROM_address..

 **************************************************************************************************/
void NVM_Init(uint16_t v_eepromAddress_u16, uint8_t v_eepromData_u8)
{
  offset = x;
}

uint16_t NVM_Offset(void x)
{
  return x-offset;
}

/***************************************************************************************************
  uint8_t NVM_Read(uint16_t v_eepromAddress_u16)
 ***************************************************************************************************
 * I/P Arguments: uint16_t: eeprom_address from where eeprom_data is to be read.
 * Return value    : uint8_t: data read from Eeprom.

 * description: This function is used to read the data from specified EEPROM_address.        
 ***************************************************************************************************/
void NVM_Read(uint16_t v_eepromAddress, uint8_t *ptr_ramAddress_u8)
{ 
  EEPROM_ReadNBytes(OFFSET(v_eepromAddress,offset), ptr_ramAddress_u8, (uint16_t)STAGES);
  EEPROM_ReadNBytes(OFFSET(v_eepromAddress,offset), ptr_ramAddress_u8+STAGES, (uint16_t)STAGES*2);
}

/***************************************************************************************************
  uint8_t NVM_Write(uint16_t v_eepromAddress_u16)
 ***************************************************************************************************
 * I/P Arguments: uint16_t: eeprom_address from where eeprom_data is to be read.
 * Return value    : uint8_t: data read from Eeprom.

 * description: This function is used to read the data from specified EEPROM_address.        
 ***************************************************************************************************/
uint8_t NVM_Write(uint16_t v_eepromAddress_u16)
{
  
  EEPROM_ReadNBytes(OFFSET(v_eepromAddress,offset), ptr_ramAddress_u8, (uint16_t)STAGES);
  EEPROM_ReadNBytes(OFFSET(v_eepromAddress,offset), ptr_ramAddress_u8+STAGES, (uint16_t)STAGES*2);
}
