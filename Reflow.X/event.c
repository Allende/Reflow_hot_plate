/***************************************************************************************************
                                  Top Level Declaration
***************************************************************************************************/
#include "enums.h"
#include "event.h"
/***************************************************************************************************
                             Commonly used LCD macros/Constants
***************************************************************************************************/
// GLCD file col
//#define LOAD_FILE 1

/**************************************************************************************************/

/***************************************************************************************************
                                 Struct/Enums used
***************************************************************************************************/

/***************************************************************************************************
Local Function Declaration
***************************************************************************************************/
void setsy( t_systems);
t_systems getsy( void );
void clrsy( void );

void seth ( t_handles);
t_handles geth ( void );
void clrh ( void );

/***************************************************************************************************
lobal Variables
***************************************************************************************************/
static t_systems *p_event_sy = NULL;
static t_handles *p_event_h  = NULL;

/************************************************************
Function Definition
*************************************************************/

void setsy(t_systems s)
{
  *p_event_sy= s;
}

t_systems getsy(void)
{
  return *p_event_sy;
}

void clrsy(void)
{
  *p_event_sy=sysNONE;
}

void seth(t_handles s)
{
  *p_event_h= s;
}

t_handles geth(void)
{
  return *p_event_h;
}

void clrh(void)
{
  *p_event_h = hanNONE;
}

void EVENT_Init(void *const this)
{

  static const t_eventVtbl vtbls = 
  {
    setsy,
    getsy,
    clrsy
  };

  static const t_eventVtbl vtblh =
  {
    seth,
    geth,
    clrh
  };

  CAST(x_event, this)->handle->vptr = &vtblh;
  p_event_h = CAST(x_event  , this )->handle->&event;
  *p_event_h = hanNONE;

  CAST(x_event, this)->system->vptr = &vtbls;
  p_event_h = CAST(x_event  , this )->handle->&event;
  *p_event_h = sysNONE;

}