/***************************************************************************************************
                                  Top Level Declaration
***************************************************************************************************/
#include "enums.h"
#include "profile.h"
#include "eeprom.h"
/***************************************************************************************************
                             Commonly used LCD macros/Constants
***************************************************************************************************/
// GLCD file col
// #define LOAD_FILE 1
/**************************************************************************************************/

/***************************************************************************************************
                                 Struct/Enums used
***************************************************************************************************/

/***************************************************************************************************
Local Function Declaration
***************************************************************************************************/
s_st_x pointer(void);
void   set(t_stage*);
s_st_x get(t_stage*);
void   clear(void);
void   clear_all(void);
void   retrieve(uint8_t,t_stage* );

/***************************************************************************************************
private global Variables
***************************************************************************************************/
static x_profile *p_profile = NULL;
/************************************************************
Function Definition
*************************************************************/

s_st_x pointer(void)
{
  return *p_profile->p;
}

void put(t_stage *this)
{
  *p_profile->p++;
  *p_profile->p->Time = *this->Time;
  *p_profile->p->Temp = *this->Temp;
}

void get(t_stage *this)
{
  *this->Time = *p_profile->p->Time;
  *this->Temp = *p_profile->p->Temp;
}

void setTemp(uint8_t idx, t_stage *this)
{
  *p_profile->stages[idx]->Temp = *this->Temp;
}

void setTime(uint8_t idx, t_stage *this)
{
  *p_profile->stages[idx]->Time = *this->Time;
}

void getTemp(uint8_t idx, t_stage *this)
{
  *this->Temp = *p_profile->stages[idx]->Temp;
}

void getTime(uint8_t idx, t_stage *this)
{
  *this->Time = *p_profile->stages[idx]->Time;
}

void clear(void)
{
  *p_profile->p--;
}

void clear_all(void)
{
  *p_profile->p = *p_profile->stages;
}

void retrieve(uint8_t idx, t_stage *this)
{  
  *this->Time = *p_profile->stages[idx]->Time;
  *this->Temp = *p_profile->stages[idx]->Temp;
}

void PROFILE_Init(void *const this)
{
  static 
  const t_profileVtb1 vtbl =
    {
      pointer,
      set,
      get,
      clear,
      clear_all
    };

  p_profile = this;
  CAST(t_profile, this)->vptr = &vtbl;
  CAST(t_profile, this)->p = NULL;
  CAST(t_profile, this)->*tail = BEGIN;
}