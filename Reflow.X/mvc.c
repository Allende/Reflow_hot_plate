/***************************************************************************************************
Top Level Declaration
***************************************************************************************************/
#include "lib/glcd_x.h"
#include "mvc.h"
#include "nvm.h"
/***************************************************************************************************
Local Function Declaration
***************************************************************************************************/

/***************************************************************************************************
MVC Functions Declaration
***************************************************************************************************/

/************************************************************
 Function Definited to MODEL
*************************************************************/
void model_action_x_load( x_model *this ,x_event *this )
{
  t_stage  stages[STAGES];
  uint8_t  stgsRaw[3*STAGES];
  uint8_t  tbf[STAGES];//file array indicator

  NVM_tbf(tbf);
  NVM_Read(this->msg ,stgsRaw );
  for (i=0;i<STAGES;i++)
  {
    if(tbf[i]){
      *(stages+i)->e = TRUE;
      *(stages+i)->Temp = *(stgsRaw+i);
      *(stages+i)->Time = *(uint16_t*)(stgsRaw+(2*i)+STAGE);      
    }else
    {
      *(stages+i)->e = FALSE;
      *(stages+i)->Temp = *(stgsRaw+i);
      *(stages+i)->Time = *(uint16_t*)(stgsRaw+(2*i)+STAGE);      
    }
  }
  while (i-- > 0)
  {
    this->model->profile->vptr->set(&stages); //state
  }
}

void model_action_x_define( x_model *this ,x_event *this )
{
  t_stage  stages[STAGES];
  uint8_t  stgsRaw[3*STAGES];

  for (i=0;i<STAGES;i++)
  {
    *(stages+i)->Temp = *(stgsRaw+i);
    *(stages+i)->Time = *(uint16_t*)(stgsRaw+(2*i)+STAGE);
  }
  while (i-- > 0 )
  {
    this->model->profile->vptr->get(&stages); //state
  }
}

void model_action_x_storage( x_model *this ,x_event *this )
{
  t_stage  stages[STAGES];
  uint8_t  s[3*STAGES];
  uint8_t  tbf[STAGES];//file array indicator

  for (i=0;i<STAGES;i++)
  {
    if(tbf[i]){
      *(stages+i)->e = TRUE;
      *(stages+i)->Temp = *(s+i);
      *(stages+i)->Time = *(uint16_t*)(s+(2*i)+STAGE);      
    }else
    {
      *(stages+i)->e = FALSE;
      *(stages+i)->Temp = *(s+i);
      *(stages+i)->Time = *(uint16_t*)(s+(2*i)+STAGE);      
    }
  }
  NVM_Write(p ,s );
  while (i-- > 0)
  {
    this->model->profile->vptr->set(&stages); //state
  }
}

void model_action_x_pannel( x_model *this ,x_event *this )
{
  t_stage  stages[STAGES];
  uint8_t  s[3*STAGES];
  s_st_x offset= A;
}

void model_action_x_temp( x_model *this ,x_event *this )
{
  t_stage  stage;
  s_st_x   st,st1;
  
  this->msg
      ->vptr
      ->retrieve(-1,&st1);

  this->profile
      ->vptr
      ->getTemp(OFFSET(st1,(s_st_x)A),&stage);
  
  this->msg
      ->vptr
      ->get(&st);// up/down

  if (st==UP)
    stage.Temp++;
  else{
    stage.Temp= (stage.Temp>0) ? stage.Temp--:0;
  }
  
  this->profile
      ->vptr
      ->setTemp(OFFSET(st1,(s_st_x)A),&stage);  
}

void model_action_x_time( x_model *this ,x_event *this )
{
  t_stage  stage;
  s_st_x   st,st1;
  
  this->msg
      ->vptr
      ->retrieve(-1,&st1);

  this->profile
      ->vptr
      ->getTime(OFFSET(st1,(s_st_x)A),&stage);
  
  this->msg
      ->vptr
      ->get(&st);// up/down

  if (st==UP)
    stage.Time++;
  else{
    stage.Time= (stage.Time>0) ? stage.Time--:0;
  }
    
  
  this->profile
      ->vptr
      ->setTime(OFFSET(st1,(s_st_x)A),&stage);  
}
/* 
  Factory MODEL Function Definition
*/

void create_model_action( x_model *this, s_st_x level)
{
  switch (level)
  {
  case BEGIN:
    //this->action = &model_action_x_begin;
    break;
  case LOAD:
    this->action = &model_action_x_load;
    break;
  case DEFINE:
    this->action = &model_action_x_define;
    break;
  case START:
    /* code
    /*change BEGIN menu button start to stop*/
    this->action = &model_action_x_start;
    break;
  case STOP:
    /* code */
    /*change BEGIN menu to request confirm stop, them change menu*/
    this->action = &model_action_x_stop;
    break;
  case END:
    /* code */
    /*shows a chart pront to inform end flow cycle */
    this->action = &model_action_x_end;
    break;
  case A:
  case B:
  case C:
  case D:
  case E:  
  case PANNEL:
    this->action = &model_action_x_pannel;
    break;
  case MENU:
    /*shows a list settings storage*/
    this->action = &model_action_x_menu;
    break;
  case SET:
    /*shows a chart with "OK" storage procedure*/
    this->action = &model_action_x_set;
    /* code */
    break;
  case FAILED:
    /*shows a chart with "FAILURE" storage procedure or other*/
    /* code */
    break;
  case PROGRESS:
    /*this it for progress operation*/
    this->action = &model_action_x_progress;
    break;
  case WHAIT:
    /*this it for a none one operation state*/
    break;
  default:
    break;
  }
}

/************************************************************
Function Definitions for VIEW
*************************************************************/
void console_display_x_begin(s_st_x st, x_model *this)
{
  static s_st_x previus = BEGIN;
  static bool init = false;
  if (previus != p)
  {
    switch (previus)
    {
    case LOAD:
      GLCD_SetCursor(LOAD_FILE, LOAD_COL);
      GLCD_Printf("STOP\n");
      break;
    case DEFINE:
      GLCD_SetCursor(DEFINE_FILE, DEFINE_COL);
      GLCD_Printf("DEFINE\n");
      break;
    case START:
      GLCD_SetCursor(START_FILE, LOAD_COL);
      GLCD_Printf("START\n");
      break;
    case STOP:
      GLCD_SetCursor(STOP_FILE, LOAD_COL);
      GLCD_Printf("STOP\n");
      break;
    }
  }
  
  GLCD_EnableInversion();
  switch (p)
  {
  case LOAD:
    GLCD_SetCursor(LOAD_FILE, LOAD_COL);
    GLCD_Printf("LOAD\n");
    break;
  case DEFINE:
    GLCD_SetCursor(DEFINE_FILE, DEFINE_COL);
    GLCD_Printf("DEFINE\n");
    break;
  case START:
    GLCD_SetCursor(START_FILE, LOAD_COL);
    GLCD_Printf("START\n");
    break;
  case STOP:
    GLCD_SetCursor(STOP_FILE, LOAD_COL);
    GLCD_Printf("STOP\n");
    // init=false;
    break;
  default: //"NONE"case 1:
    if (!init)
    {
      GLCD_DisableInversion();
      GLCD_ScrollMessage(0, "Welcome to Reflow again");
      GLCD_Printf("\n");
      GLCD_GoToLine(1);
      GLCD_EnableInversion();
      GLCD_Printf("        LOAD        \n");
      GLCD_DisableInversion();
      GLCD_SetCursor(2, 8);
      GLCD_Printf("DEFINE\n");
      GLCD_SetCursor(3, 8);
      GLCD_Printf("START\n");
      GLCD_SetCursor(6, 0);
      GLCD_Printf(BAR);
      GLCD_Printf(CURSORS);
      init++;
    }
    break;
  }
  GLCD_DisableInversion();
  previus = p;
  return;
}

void console_display_x_load(s_st_x st, x_model *this)
{
  static uint8_t previus = 0;
  static bool init = false;
  /* clear previus highlight item */
  if ( previus != x->x)
  {
    GLCD_SetCursor(previus, LOAD_COL);
    GLCD_Printf("SETTING ");
    GLCD_Printf(previus);
  }
  /* Set current highlight item */
  GLCD_EnableInversion();
  switch (p)
  {
  case UP:
  case DOWN:    
    GLCD_SetCursor(x->x, LOAD_COL);
    GLCD_Printf("SETTING ");
    GLCD_Printf(x->x);
    break;
  default: //"NONE"case 1:
    if (!init)
    {
      GLCD_DisableInversion();
      GLCD_ScrollMessage(0, "Welcome to Reflow again");
      GLCD_Printf("\n");
      GLCD_GoToLine(1);
      GLCD_EnableInversion();
      GLCD_Printf("      SETTING 0     \n");
      GLCD_DisableInversion();
      GLCD_SetCursor(2, LOAD_COL);
      GLCD_Printf("SETTING 1\n");
      GLCD_SetCursor(3, LOAD_COL);
      GLCD_Printf("SETTING 2\n");
      GLCD_SetCursor(4, LOAD_COL);
      GLCD_Printf("SETTING 3\n");
      GLCD_SetCursor(5, LOAD_COL);
      GLCD_Printf("SETTING 4\n");
      GLCD_SetCursor(6, 0);
      GLCD_Printf(BAR);
      GLCD_Printf(CURSORS);
      init++;
    }
    break;
  }
  GLCD_DisableInversion();
  previus = x->x;
  return;
}

void console_display_x_define(s_st_x st, x_model *this)
{
  static s_st_x previus = BEGIN;
  static bool init = false;
  // Clean Screen high light
  if (previus != p)
  {
    switch (previus)
    {
    case A:
      GLCD_SetCursor(A_FILE, A_COL);
      GLCD_Printf("A\n");
      break;
    case B:
      GLCD_SetCursor(B_FILE, B_COL);
      GLCD_Printf("B\n");
      break;
    case C:
      GLCD_SetCursor(C_FILE, C_COL);
      GLCD_Printf("C\n");
      break;
    case D:
      GLCD_SetCursor(D_FILE, D_COL);
      GLCD_Printf("D\n");
      break;
    case E:
      GLCD_SetCursor(E_FILE, E_COL);
      GLCD_Printf("E\n");
      break;
    }
  }
  GLCD_EnableInversion();
  switch (p)
  {
  case A:
    GLCD_SetCursor(A_FILE, A_COL);
    GLCD_Printf("A\n");
    break;
  case B:
    GLCD_SetCursor(B_FILE, B_COL);
    GLCD_Printf("B\n");
    break;
  case C:
    GLCD_SetCursor(C_FILE, C_COL);
    GLCD_Printf("C\n");
    break;
  case D:
    GLCD_SetCursor(D_FILE, D_COL);
    GLCD_Printf("D\n");
    // init=false;
    break;
  case E:
    GLCD_SetCursor(E_FILE, E_COL);
    GLCD_Printf("E\n");
    break;
  default: //"NONE"case 1:
    if (!init)
    {
      GLCD_DisableInversion();
      GLCD_ScrollMessage(0, "Define reflow curve");
      GLCD_Printf("\n");
      GLCD_SetCursor(A_FILE, A_COL);
      GLCD_EnableInversion();
      GLCD_Printf("A\n");
      GLCD_DisableInversion();
      GLCD_SetCursor(B_FILE, B_COL);
      GLCD_Printf("B\n");
      GLCD_SetCursor(C_FILE, C_COL);
      GLCD_Printf("C\n");
      GLCD_SetCursor(D_FILE, D_COL);
      GLCD_Printf("D\n");
      GLCD_SetCursor(E_FILE, E_COL);
      GLCD_Printf("E\n");
      GLCD_SetCursor(6, 0);
      GLCD_Printf(BAR);     //"----------------------\n"
      GLCD_Printf(CURSORS); //"<:out >:int v:up v:down\n"
      init++;
    }
    break;
  }
  GLCD_DisableInversion();
  previus = p;
  return;
}

void console_display_x_end(s_st_x st, x_model *this)
{
  static s_st_x previus = END;
  static bool init = false;
  // Clean Screen high light
  if (previus != p)
  {
    GLCD_DisableInversion();
    GLCD_ScrollMessage(0, "Welcome to Reflow again");
    GLCD_SetCursor(END_FILE, END_COL);
    GLCD_EnableInversion();
    GLCD_Printf("       ENDED        \n");
    GLCD_DisableInversion();
    GLCD_SetCursor(BOT_FILE, 0);
    GLCD_Printf(BAR);
    GLCD_Printf(CURSORS);
    init++;
    previus = p;
  }
  return;
}

void console_display_x_start(s_st_x st, x_model *this)
{
  static s_st_x previus = STOP;
  static bool init = false;
  // Clean Screen high light
  if (previus != p)
  {
    GLCD_DisableInversion();
    GLCD_ScrollMessage(0, "Welcome to Reflow again");
    GLCD_Printf("\n");
    GLCD_GoToLine(1);
    GLCD_Printf("LOAD\n");
    GLCD_SetCursor(2, 8);
    GLCD_Printf("DEFINE\n");
    GLCD_SetCursor(3, 8);
    GLCD_EnableInversion();
    GLCD_Printf("        STOP        \n");
    GLCD_DisableInversion();
    GLCD_SetCursor(6, 0);
    GLCD_Printf(BAR);
    GLCD_Printf(CURSORS);
    init++;
    previus = p;
    Delay_1000();
  }
  return;
}

void console_display_x_stop(s_st_x st, x_model *this)
{
  static s_st_x previus = START;
  static bool init = false;
  // Clean Screen high light
  if (previus != p)
  {
    GLCD_DisableInversion();
    GLCD_ScrollMessage(0, "Welcome to Reflow again");
    GLCD_Printf("\n");
    GLCD_GoToLine(1);
    GLCD_Printf("LOAD\n");
    GLCD_SetCursor(A_FILE, A_FILE);
    GLCD_Printf("DEFINE\n");
    GLCD_SetCursor(3, 8);
    GLCD_EnableInversion();
    GLCD_Printf("       START        \n");
    GLCD_DisableInversion();
    GLCD_SetCursor(6, 0);
    GLCD_Printf(BAR);
    GLCD_Printf(CURSORS);
    init++;
    previus = p;
  }
  return;
}

void console_display_x_confirm(s_st_x st, x_model *this)
{
  static s_st_x previus = NONE;
  static bool init = false;
  // Clean Screen high light

  if (previus != p)
  {
    switch (previus)
    {
    case OK:
      GLCD_SetCursor(A_FILE, A_COL);
      GLCD_Printf("OK\n");
      break;
    case CANCEL:
      GLCD_SetCursor(B_FILE, B_COL);
      GLCD_Printf("CANCEL\n");
      break;
    }
  }

  if (previus != p)
  {
    GLCD_EnableInversion();
    switch (p)
    {
    case OK:
      GLCD_SetCursor(A_FILE, A_COL);
      GLCD_Printf("OK\n");
      break;
    case CANCEL:
      GLCD_SetCursor(B_FILE, B_COL);
      GLCD_Printf("CANCEL\n");
      break;
    default: //"NONE"case 1:
      if (!init)
      {
        GLCD_DisableInversion();
        GLCD_ScrollMessage(0, "END the Reflow process?");
        GLCD_Printf("\n");
        GLCD_GoToLine(1);
        GLCD_Printf("OK\n");
        GLCD_SetCursor(2, 8);
        GLCD_EnableInversion();
        GLCD_Printf("CANCEL\n");
        GLCD_DisableInversion();
        GLCD_SetCursor(6, 0);
        GLCD_Printf(BAR);
        GLCD_Printf(CURSORS);
        init++;
        previus = CANCEL;
      }
      break;
    }
  }
  return;
}

void console_display_x_pannel( s_st_x st, x_model *this)
{
  static s_st_x previus = TEMP;
  static bool init = false;
  t_stage x;
  // Clean Screen high light
  if (previus != st)
  {
    switch (previus)
    {
    case TEMP:
      GLCD_SetCursor(TEMP_FILE, TEMP_COL);
      GLCD_Printf("°C\n");
      break;
    case GAP:
      GLCD_SetCursor(GAP_FILE, GAP_COL);
      GLCD_Printf("Seg\n");
      break;
    }
  }

  *this->profile->vptr->retrieve(OFFSET(st,A),&x);

  GLCD_EnableInversion();
  switch (st)
  {
  case TEMP:
    GLCD_SetCursor(TEMP_FILE, TEMP_COL);
    GLCD_Printf("°C\n");
    GLCD_DisableInversion();
    GLCD_SetCursor(TEMPX_FILE, TEMPX_COL);
    GLCD_Printf(x.Temp);
    break;
  case GAP:
    GLCD_SetCursor(GAP_FILE, GAP_COL);
    GLCD_Printf("Seg\n");
    GLCD_DisableInversion();
    GLCD_SetCursor(GAPX_FILE, GAPX_COL);
    GLCD_Printf(x.Time);
    break;
  default: //"NONE"case 1:
    if (!init)
    {
      GLCD_DisableInversion();
      GLCD_ScrollMessage(0, "Define reflow curve");
      GLCD_Printf("\n");
      GLCD_EnableInversion();
      GLCD_SetCursor(TEMP_FILE, TEMP_COL);
      GLCD_Printf("°C\n");
      GLCD_DisableInversion();
      GLCD_SetCursor(TEMPX_FILE, TEMPX_COL);
      GLCD_Printf("0");
      GLCD_SetCursor(GAP_FILE, GAP_COL);
      GLCD_Printf("Seg\n");
      GLCD_SetCursor(GAPX_FILE, GAPX_COL);
      GLCD_Printf("0");
      GLCD_SetCursor(6, 0);
      GLCD_Printf(BAR);     //"----------------------\n"
      GLCD_Printf(CURSORS); //"<:out >:int v:up v:down\n"
      init++;
    }
    break;
  }
  GLCD_DisableInversion();
  while (i < length(table))
    GLCD_curve_OR(table[i][1], table[i][2]);

  previus = st;
  return;
}

void console_display_x_temp(s_st_x st, x_model *this)
{
  static s_st_x previus = NONE;
  static bool   init = false;
  t_stage       x;
  
  *this->profile->vptr->retrieve(OFFSET(st,A),&x);

  // Clean Screen high light
  if (previus == p)
  {
    GLCD_SetCursor(TEMPX_FILE, TEMPX_COL);
    GLCD_Printf(x.Temp);
  }

  if (!init)
  {
    GLCD_SetCursor(TEMPX_FILE, TEMPX_COL);
    GLCD_Printf("0");
    init++;
    previus = p;
  }
  return;
}

void console_display_x_gap(s_st_x st, x_model *this)
{
  static s_st_x previus = NONE;
  static bool init = false;
  t_stage       x;
  
  *this->profile->vptr->retrieve(OFFSET(st,A),&x);
  // Clean Screen high light
  if (previus == p)
  {
    GLCD_SetCursor(GAPX_FILE, GAPX_COL);
    GLCD_Printf(x.Time);
  }

  if (!init)
  {
    GLCD_SetCursor(GAPX_FILE, GAPX_COL);
    GLCD_Printf("0");
    init++;
    previus = p;
  }
  return;
}

void console_display_x_progress(s_st_x st, x_model *this)
{
  static byte previus = 0;
  static bool init = false;
  byte l, i = 0;
  //
  switch (p)
  {
  case PROGRESS:
    l = strlen(x->v);
    if ((previus != l) && (l <= NSAMPLEGLCD))
    {
      do
      {
        GLCD_hist_line(i, (x->*(v++)), INIT_LINE, END_LINE);
        i++;
      } while (i++ < l);
      previus = l;

      GLCD_SetCursor(A_FILE, A_COL);
      GLCD_Printf("A");
      GLCD_SetCursor(B_FILE, B_COL);
      GLCD_Printf("B");
      GLCD_SetCursor(C_FILE, C_COL);
      GLCD_Printf("C");
      GLCD_SetCursor(D_FILE, D_COL);
      GLCD_Printf("D");
      GLCD_SetCursor(E_FILE, E_COL);
      GLCD_Printf("E");
    }
    if (!init)
    {
      GLCD_Clear;
      GLCD_ScrollMessage(0, "Progress reflow operation");
      // GLCD_Printf("\n");
      GLCD_SetCursor(A_FILE, A_COL);
      GLCD_Printf("A");
      GLCD_SetCursor(B_FILE, B_COL);
      GLCD_Printf("B");
      GLCD_SetCursor(C_FILE, C_COL);
      GLCD_Printf("C");
      GLCD_SetCursor(D_FILE, D_COL);
      GLCD_Printf("D");
      GLCD_SetCursor(E_FILE, E_COL);
      GLCD_Printf("E");

      GLCD_SetCursor(6, 0);
      GLCD_Printf(BAR);     //"----------------------\n"
      GLCD_Printf(CURSORS); //"<:out"
      init++;
    }
    break;
  default:
    /*STOP SIGNAL*/
    previus = 0;
    init = false;
    break;
  }
  return;
}

/* 
  Factory VIEW Function Definitions
 */

void create_console_view(x_view *this, s_st_x level)
{
  switch (level)
  {
  case BEGIN:
    this->display->f_void_x  = &console_display_x_begin;
    break;
  case LOAD:
    this->display->f_void_x = &console_display_x_load;
    break;
  case DEFINE:
    this->display->f_void_x = &console_display_x_define;
    break;
  case STORAGE:
    this->display->f_void_x = &console_display_x_storage;
    break;
  case START:
    /* code
    /*change BEGIN menu button start to stop*/
    this->display->f_void_x = &console_display_x_start;
    break;
  case STOP:
    /* code */
    /*change BEGIN menu to request confirm stop, them change menu*/
    this->display->f_void_x = &console_display_x_stop;
    break;
  case END:
    /* code */
    /*shows a chart pront to inform end flow cycle */
    this->display->f_void_x = &console_display_x_end;
    break;
  case A:
  case B:
  case C:
  case D:
  case E:
  case PANNEL:
    this->display->f_void_x = &console_display_x_pannel;
    break;
  case MENU:
    /*shows a list settings storage*/
    this->display->f_void_x = &console_display_x_menu;
    break;
  case SET:
    /*shows a chart with "OK" storage procedure*/
    this->display->f_void_x = &console_display_x_set;
    /* code */
    break;
  case FAILED:
    /*shows a chart with "FAILURE" storage procedure or other*/
    /* code */
    break;
  case PROGRESS:
    /*this it for progress operation*/
    this->display->f_void_x = &console_display_x_progress;
    break;
  case WHAIT:
    /*this it for a none one operation state*/
    break;
  default:
    break;
  }
}


/************************************************************
Function Definited to CONTROLLER
*************************************************************/

void controller_update_data(x_controller *this, int x)
{
  this->model->action->f_void_x(&this->model, this->event);
  this->view->display->f_void_x(&this->model, this->event);
}

void x_controler_init(x_controller *this, x_model *model, x_view *view, x_event *event)
{
  this->model = model;
  this->view  = view;
  this->event = event;
}

void menu_controler(x_controller *this)
{
  s_st_x level;
  t_handles handle;
  bool l = TRUE;
  //if (this->event->change())
  level = this->view->level->get();
  handle = this->event->handle->get();
  switch (level)
  {
  case BEGIN:
    //create_console_view(&this->view, level);
    //controller_update_data(x_controller * this, NULL);
    if (this->event->handle->change())
    {
      //this->view->level->set(level);
      switch (handle)
      {
        case UP:
          switch (this->view->state->get())
          {
          case LOAD:
            if (this->view->level->get() == START)
              this->view->state->set(STOP);
            else
              this->view->state->set(START);
            break;
          case DEFINE:
            this->view->state->set(LOAD);
            break;
          case START:
            this->view->state->set(DEFINE);
            break;
          case STOP:
            this->view->state->set(DEFINE);
            break;
          }
          break;
        case DOWN:
          switch (this->view->state->get())
          {
          case LOAD:
            this->view->state->set(DEFINE);
            break;
          case DEFINE:
            if (this->view->level->get() == START)
              this->view->state->set(STOP);
            else
              this->view->state->set(START);
            break;
          case START:
            this->view->state->set(LOAD);
            break;
          case STOP:
            this->view->state->set(LOAD);
            break;
          }
          break;
        case OUT:
          /*There not be a sense, have not a super class or inherance*/
          // this->view->level->clear();
          break;
        case INTRO:
          this->view->level->set(this->view->state->get());
          break;
        default:
          break;
      }      
      create_console_view(this->view, level);
      controller_update_data(this, NULL);
    } 
    break;
  case LOAD:
    /* bring some configuration from eeprom v[[T][time]]*/
    if (this->event->handle->change())
    {
      //this->view->level->set(level;
      switch (handle)
      {
        case UP:
          switch (this->view->state->get())
          {
            case BANK0:
              this->view->state->set(BANK3);
              break;
            case BANK1:
              this->view->state->set(BANK0);
              break;
            case BANK2:
              this->view->state->set(BANK1);
              break;
            case BANK3:
              this->view->state->set(BANK2);
              break;
            default:
              this->view->state->set(BANK0);
            break;
          }
          break;
        case DOWN:
          switch (this->view->state->get())
          {
            case BANK0:
              this->view->state->set(BANK1);
              break;
            case BANK1:
              this->view->state->set(BANK2);
              break;
            case BANK2:
              this->view->state->set(BANK3);
              break;
            case BANK3:
              this->view->state->set(BANK0);
              break;
            default:
              this->view->state->set(BANK0);
            break;
          }
          break;
        case OUT:
          this->view->level->clear();
          break;
        case INTRO:                    
          this->view->level->set(CONFIRM);
          this->model->msg->set(this->view->state->get());
          return;
          break;
        default:
          return; 
          break;
      }
      create_console_view( this->view,  level);
      create_model_action( this->model, level);
      controller_update_data( this, NULL);
    }
    break;
  case DEFINE:
    /* Set some configuration to eeprom v[[T][time]]*/
    create_console_view(&this->view, level);
    controller_update_data(x_controller * this, null);
    this->view->level->set(level);
    switch (handle)
    {
    case UP:
      switch (this->view->state->get())
      {
      case A:
        this->view->state->set(B);
        break;
      case B:
        this->view->state->set(C);
        break;
      case C:
        this->view->state->set(D);
        break;
      case D:
        this->view->state->set(E);
        break;
      case E:
        this->view->state->set(A);
        break;
      }
      break;
    case DOWN:
      switch (this->view->state->get())
      {
      case A:
        this->view->state->set(E);
        break;
      case B:
        this->view->state->set(A);
        break;
      case C:
        this->view->state->set(B);
        break;
      case D:
        this->view->state->set(C);
        break;
      case E:
        this->view->state->set(D);
        break;
      }
      break;
    case OUT:
      this->view->level->set(CONFIRM);
      this->view->state->set(CANCEL);  //init highlight line
      break;
    case INTRO:
      this->view->level->set(this->view->state->get());//(PANNEL)
      this->view->level->set(PANNEL);
      this->model->msg->set(this->view->state->get());//[A]
      //this->view->level->set(this->view->state->get());
      break;
    default:
      break;
    }
    break;

  case START:
    create_console_view(&this->view, level);
    controller_update_data(x_controller * this, null);
    this->event->clear();
    this->view->level->set(PROGRESS);
    this->view->state->set(PROGRESS);
    break;

  case STORAGE:
    /* bring some configuration from eeprom v[[T][time]]*/
    if (this->event->handle->change())
    {
      //this->view->level->set(level;
      switch (handle)
      {
        case UP:
          switch (this->view->state->get())
          {
            case BANK0:
              this->view->state->set(BANK3);
              break;
            case BANK1:
              this->view->state->set(BANK0);
              break;
            case BANK2:
              this->view->state->set(BANK1);
              break;
            case BANK3:
              this->view->state->set(BANK2);
              break;
            default:
              this->view->state->set(BANK0);
            break;
          }
          break;
        case DOWN:
          switch (this->view->state->get())
          {
            case BANK0:
              this->view->state->set(BANK1);
              break;
            case BANK1:
              this->view->state->set(BANK2);
              break;
            case BANK2:
              this->view->state->set(BANK3);
              break;
            case BANK3:
              this->view->state->set(BANK0);
              break;
            default:
              this->view->state->set(BANK0);
            break;
          }
          break;
        case OUT:
          this->view->level->clear();//BEGIN
          break;
        case INTRO:        
          //this->view->level->clear();
          break;
        default:
          return; 
          break;
      }
      create_console_view(this->view, level);
      create_model_action(this->model, level, statelevel);
      controller_update_data(this, NULL);
    }
    break;  
  case STOP:
    create_console_view(&this->view, level);
    controller_update_data(x_controller * this, null);
    switch (handle)
    {
      case UP:
      case DOWN:
        switch (this->view->state->get())
        {
        case STOP:
          this->view->state->set(PROGRESS);
          break;
        case PROGRESS:
          this->view->state->set(STOP);
          break;
        }
        break;
      case OUT:
        break;
      case INTRO:
        switch (this->view->state->get())
        {
        case STOP:
          this->view->state->set(CONFIRM);
          break;
        case PROGRESS:
          this->view->state->set(PROGRESS);
          this->view->level->set(PROGRESS);
          break;
        }
        break;
      default:
        break;
    }
    this->event->clear();
    break;
  case PROGRESS:
    /* code */
    // Not run antil 1000 seg, but de core must to run
    // signal = time_now()-time_stamp;
    // l=(signal>1000)?true:false;
    if (l)
    {
      create_console_view(&this->view, level);
      controller_update_data(x_controller * this, null);
    }
    if (init && this->event->get_sys() == SAMPLE)
      controller_update_data(x_controller * this, null);

    if (h == OUT)
    {
      this->event->clear();
      this->view->level->clear();
      this->view->state->set(STOP);
      l = false;
    }
    break;
  case END:
    /* System Signal */
    create_console_view(&this->view, level);
    controller_update_data(x_controller * this, null);
    if (h == OUT)
    {
      this->event->clear();
      this->view->level->clear_all();
      this->view->state->set(BEGIN);
    }
    break;
  case A:
  case B:
  case C:
  case D:
  case E:
  case PANNEL:
    /* code */
    create_console_view(&this->view, level);
    controller_update_data(x_controller * this, null);
    switch (handle)
    {
    case UP:
    case DOWN:
      switch (this->view->state->get())
      {
      case TEMP:
        this->view->state->set(GAP);
        break;
      case GAP:
        this->view->state->set(TEMP);
        break;
      }
      break;
    case OUT:
      this->view->level->set(CONFIRM);      
      break;
    case INTRO:
      switch (this->view->state->get())
      {
      case TEMP:
        //this->view->state->set(TEMP);
        this->view->level->set(TEMP);//MSG[A] remains
        break;
      case GAP:
        //this->view->state->set(GAP);
        this->view->level->set(GAP); //MSG[A] remains
        break;
      }
      break;
    default:
      break;
    }
    break;
  case TEMP:
    /* code */
    create_console_view(&this->view, level);
    controller_update_data(x_controller * this, null);
    switch (handle)
    {
    case UP:
      this->view->state->set(UP);
      this->model->msg->put(UP);
      create_model_action(this->model, level);
      break;
    case DOWN:
      this->view->state->set(DOWN);
      this->model->msg->put(DOWN);
      create_model_action(this->model, level);
      break;
    case OUT:
      //Out without save
      this->view->state->set(TEMP);
      this->view->level->clear(); // go to to pannel
      break;
    case INTRO:
      this->view->state->set(TEMP);
      this->view->level->clear(); // go to to pannel
      //Out with save      
      break;
    default:
      break;
    }
    create_console_view(&this->view, level);
    controller_update_data(x_controller * this, null);
    break;
  case GAP:
    /* code */
    create_console_view(&this->view, level);
    controller_update_data(x_controller * this, null);
    switch (handle)
    {
    case UP:
    case DOWN:
      this->view->state->set(GAP);
      break;
    case OUT:
      this->view->state->set(GAP);
      this->view->level->clear(); // go to to pannel
      break;
    case INTRO:

      break;
    default:
      break;
    }
    break;
  case MENU:
    /* code */
    break;
  case SET:
    /* code */
    break;
  case CONFIRM:
    s_st_x bk, bk1, bk2;
    //create_console_view(&this->view, level);    
    bk = this->view->level->get(); //BEGING>X>CONFIRM
    this->view->level->clear()
    bk1= this->view->level->get(); //BEGING>X
    this->view->level->set(bk);
    
    switch (bk1) //previus level
    {
    case LOAD:
      switch (handle)
      {
      case UP:
      case DOWN:
        switch (this->view->state->get())
        {
        case OK:
          this->view->state->set(CANCEL);
          break;
        case CANCEL:
          this->view->state->set(OK);
          break;
        }
        break;
      case OUT:
        this->view->level->clear();       //BEGING>LOAD
        //this->view->level->clear();       //BEGING
        //this->view->state->set(START);    //highlight
        break;
      case INTRO:
        switch (this->view->state->get())
        {
        case OK:
          create_model_action(this->model, level);
          create_console_view(&this->view, level);
          controller_update_data(x_controller * this, null);

          this->view->level->clear_all(); //BEGING>
          this->view->state->set(START);  
          //this->view->state->set(BANK0);//
          break;
        case CANCEL:
          this->view->state->set();
          break;
        }
        break;
      default:
        break;
      }
      break;  
    case DEFINE:
      switch (handle)
      {
      case UP:
      case DOWN:
        switch (this->view->state->get())
        {
        case OK:
          this->view->state->set(CANCEL);
          break;
        case CANCEL:
          this->view->state->set(OK);
          break;
        }
        break;
      case OUT:
        this->view->level->clear();       //BEGING>DEFINE
        this->view->level->clear();       //BEGING
        this->view->state->set(START);    //highlight
        break;
      case INTRO:
        switch (this->view->state->get())
        {
        case OK:
          this->view->level->clear();     //BEGING>
          this->view->level->set(STORAGE);//BEGING>STORAGE
          //this->view->state->set(BANK0);//
          break;
        case CANCEL:
          this->view->state->set();
          break;
        }
        break;
      default:
        break;
      }
      break;
    case STOP:
      switch (handle)
      {
      case UP:
      case DOWN:
        switch (this->view->state->get())
        {
        case OK:
          this->view->state->set(CANCEL);
          break;
        case CANCEL:
          this->view->state->set(OK);
          break;
        }
        break;
      case OUT:
        this->view->state->set(STOP);
        this->view->level->clear();
        break;
      case INTRO:
        switch (this->view->state->get())
        {
        case OK:
          this->view->state->set(END);
          this->view->level->clear();
          break;
        case CANCEL:
          this->view->state->set(STOP);
          break;
        }
        break;
      default:
        break;
    }
      break;  
    
    default:
      break;
    }
    
    create_model_action(this->model, level);
    create_console_view(&this->view, level);
    controller_update_data(x_controller * this, null);
    break;
  case FAILED:
    /* code */
    break;
  case WHAIT:
    /* code */
    break;
  default:
    break;
  }

}