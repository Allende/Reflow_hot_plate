/***************************************************************************************************
                                  Top Level Declaration
***************************************************************************************************/
#include "enums.h"
#include "List.h"
/***************************************************************************************************
                             Commonly used LCD macros/Constants
***************************************************************************************************/
// GLCD file col
//#define LOAD_FILE 1

/**************************************************************************************************/

/***************************************************************************************************
                                 Struct/Enums used
***************************************************************************************************/

/***************************************************************************************************
Local Function Declaration
***************************************************************************************************/
s_st_x pointer ( void );
void set       ( s_st_x);
s_st_x get     ( void );
void clear     ( void );
void clear_all ( void );

/***************************************************************************************************
lobal Variables
***************************************************************************************************/
static t_list *p_list=NULL;
/************************************************************
Function Definition
*************************************************************/

s_st_x pointer  ( void )
{
  return *p_list->p;
}

void put      (s_st_x st)
{
  p_list->*(p++) = st;
  //p_list->p++;
}

void set      (s_st_x st)
{
  //p_list->p--;
  p_list->*(p-1) = st;
  //p_list->p++;
}

void get    (s_st_x *st )
{
  *st= p_list->*(p-1);
}

void guit    (s_st_x *st )
{
  *st= p_list->*(--p);
}

void retrieve(uint8_t idx, s_st_x *st)
{  
  *st = p_list->tail[idx];
}

void clear    (void)
{
  p_list->p--;
}

void clear_all(void)
{
  p_list->p = p_list->&tail;
}

void LIST_Init( void *const this)
{
  static const t_listVtb1 vtbl = 
  {
    pointer,
    put,
    set,
    get,
    quit,
    retrieve,
    clear,
    clear_all
  };

  p_list = this;
  CAST(t_list, this)->vptr= &vtbl;
  CAST(t_list, this)->p= NULL;
  CAST(t_list, this)->*tail= BEGIN;
}